#include <asf.h>

#include <common.h>
#include <common_system.h>
#include <common_pinmux.h>
#include <common_debug.h>
#include <common_clock.h>
#include <common_console.h>
#include <common_time.h>
#include <device_at_device_test_root_samd.h>

static int g_running;

struct bootloader_module {
	const char *name;
	int(*init)(void);
	int(*process)(void);
	void(*uninit)(void);
};

enum {
        BOOTLOADER_STATE_TEST_START,
	BOOTLOADER_STATE_TEST_FINISH
};

struct bootloader_module g_modules_00[]={
		{"system" ,synapse_system_init ,synapse_system_process ,synapse_system_uninit },
		{"clock"  ,synapse_clock_init  ,synapse_clock_process  ,synapse_clock_uninit  },
		{"console",synapse_console_init,synapse_console_process,synapse_console_uninit},
		{"time"   ,synapse_time_init   ,synapse_time_process   ,synapse_time_uninit   },
		{"at_device"  ,synapse_at_device_init,synapse_at_device_process,synapse_at_device_uninit},
};

static unsigned int g_bootloader_state;

int main (void)
{
	uint32_t m = 0;
	uint32_t iteration = 0;

    for (m = 0; m < sizeof(g_modules_00) / sizeof(g_modules_00[0]); m++) {
            g_modules_00[m].init();
    }

    g_running = 1;

    g_bootloader_state = BOOTLOADER_STATE_TEST_START;


	for(iteration=0; g_running!=0; iteration++)
	{

	    unsigned int atDeviceState;
            unsigned int atDeviceStateStatus;

	    for (m = 0; m < sizeof(g_modules_00) / sizeof(g_modules_00[0]); m++) {
	            g_modules_00[m].process();
	    }

                atDeviceState = synapse_at_device_get_state();
                atDeviceStateStatus = synapse_at_device_get_state_status();

		//synapse_infof("Hello world!");
                if (g_bootloader_state == BOOTLOADER_STATE_TEST_START) {
                        if (atDeviceState == BOOTLOADER_STATE_TEST_START) {
                                if (atDeviceStateStatus == SYNAPSE_AT_DEVICE_STATE_STATUS_SUCCESS) {
					synapse_infof("test success!");
					g_bootloader_state = BOOTLOADER_STATE_TEST_FINISH;
                                } else if (atDeviceStateStatus == SYNAPSE_AT_DEVICE_STATE_STATUS_ERROR) {
                                        synapse_errorf("at device state test error");
                                        goto reset;
                                }
                        } else {
                                synapse_errorf("unknown at device state: %d", atDeviceState);
                                goto reset;
                        }
                }
                if (g_bootloader_state == BOOTLOADER_STATE_TEST_FINISH) {
                }



	}

    for (m = 0; m < sizeof(g_modules_00) / sizeof(g_modules_00[0]); m++) {
            g_modules_00[m].uninit();
    }

reset:
	synapse_infof("bad reset :(");

}
