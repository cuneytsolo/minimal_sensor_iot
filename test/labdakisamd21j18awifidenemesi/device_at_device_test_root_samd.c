/*
	MAKEFILE MODIFICATIONS:

		Append device_x_at_test.o to # project code objects		
	
	MAIN.C MODIFICATIONS:

		Include header: #include <device_at_device_test_root_samd.h>

		Add States after g_running:
			enum {
			        BOOTLOADER_STATE_TEST_START,
				BOOTLOADER_STATE_TEST_FINISH
			};

		Add modules   : {"at_device"  ,synapse_at_device_init,synapse_at_device_process,synapse_at_device_uninit},

		Add variable after modules : static unsigned int g_bootloader_state;

		Initialize state after g_running : g_bootloader_state = BOOTLOADER_STATE_TEST_START;

		Add variable after iteration for:
			unsigned int atDeviceState;
                	unsigned int atDeviceStateStatus;
	
		Add two initializer after 'process for':
                	atDeviceState = synapse_at_device_get_state();
                	atDeviceStateStatus = synapse_at_device_get_state_status();		

		Add your if state block(s):
	                if (g_bootloader_state == BOOTLOADER_STATE_TEST_START) {
        	                if (atDeviceState == BOOTLOADER_STATE_TEST_START) {
        	                        if (atDeviceStateStatus == SYNAPSE_AT_DEVICE_STATE_STATUS_SUCCESS) {
						synapse_infof("test success!");
						g_bootloader_state = BOOTLOADER_STATE_TEST_FINISH;
        	                        } else if (atDeviceStateStatus == SYNAPSE_AT_DEVICE_STATE_STATUS_ERROR) {
        	                                synapse_errorf("at device state test error");
        	                                goto reset;
        	                        }
        	                } else {
        	                        synapse_errorf("unknown at device state: %d", atDeviceState);
        	                        goto reset;
        	                }
        	        }
        	        if (g_bootloader_state == BOOTLOADER_STATE_TEST_FINISH) {
        	        }

		Add bad reset if you don't have:
			reset:
				synapse_infof("bad reset :(");

	DEVICE_AT_DEVICE_TEST_ROOT_SAMD MODIFICATIONS:

		Shutdown State: There are 4 commented out PIN_PA00 line. Check and change as you wish!

		USART Module  : This is arranged to be compatible with SAMD21J18A. Please change pins & sercom by your mcu!

		carefull about at commands:
			esp8266: request: "AT\r\n" response:"AT\r\n\r\nOK\r\n"   after ate response:     ???
			gl865  : request: "AT\r"   response:"AT\r\r\nOK\r\n"     after ate response: "\r\nOK\r\n"

		carefull about some commented out functions:
			at_device_dec_step
			at_device_wait_response_hasstart
			at_device_wait_response_end
*/

#undef SYNAPSE_DEBUG_MODULE
#define SYNAPSE_DEBUG_MODULE SYNAPSE_DEBUG_MODULE_AT_DEVICE

#include <asf.h>

#include "common.h"
#include "common_debug.h"
#include "common_pinmux.h"
#include "common_time.h"
#include "device_at_device_test_root_samd.h"

#include <stdarg.h>
#include <sys/time.h>
#include <string.h>

struct at_device_step {
        const char *name;
        int (*process) (void);
};

struct at_device_operation {
        const char *name;
        struct at_device_step **steps;
        unsigned int nsteps;
};

struct at_device_state {
        const char *name;
        struct at_device_operation **operations;
        unsigned int noperations;
};

static unsigned int g_at_device_state;
static unsigned int g_at_device_state_status;
static unsigned int g_at_device_operation;
static unsigned int g_at_device_step;
static unsigned int g_at_device_pstep;
static struct timeval g_at_device_state_timeval;
static struct timeval g_at_device_operation_timeval;
static struct timeval g_at_device_step_timeval;

static struct usart_module g_at_device_uart_module;

#define SYNAPSE_AT_DEVICE_DOWNLOAD_BUFFER_SIZE        (1024)
#define SYNAPSE_AT_DEVICE_UPLOAD_BUFFER_SIZE          (512)

#define SYNAPSE_AT_DEVICE_INPUT_BUFFER_SIZE           (2 * 1024)
static uint8_t g_at_device_input_buffer[SYNAPSE_AT_DEVICE_INPUT_BUFFER_SIZE];

#define SYNAPSE_AT_DEVICE_OUTPUT_BUFFER_SIZE          (1024)
static uint8_t g_at_device_output_buffer[SYNAPSE_AT_DEVICE_OUTPUT_BUFFER_SIZE];

#define SYNAPSE_AT_DEVICE_OUTPUT_BUFFER_SIZE          (1024)
//WORKS! static char g_at_device_output_str_buffer[SYNAPSE_AT_DEVICE_OUTPUT_BUFFER_SIZE]="{\"count\": \"55\", \"temperature\": \"55\", \"humidity\": \"55\"}";
//WORKS! static char g_at_device_output_str_buffer[SYNAPSE_AT_DEVICE_OUTPUT_BUFFER_SIZE]="GET / HTTP/1.1\r\nHost: 34.67.16.50\r\n\r\n";
static char g_at_device_output_str_buffer[SYNAPSE_AT_DEVICE_OUTPUT_BUFFER_SIZE]="POST / HTTP/1.1\r\nHost: 34.67.16.50\r\nContent-Type: application/json\r\nContent-Length:11\r\n\r\n{\"c\": \"55\"}\r\n\r\n";

/*
static int at_device_dec_step (void)
{
        gettimeofday(&g_at_device_step_timeval, NULL);
        g_at_device_pstep  = g_at_device_step;
        g_at_device_step  -= 1;
        return 0;
}
*/

static int at_device_inc_step (void)
{
        gettimeofday(&g_at_device_step_timeval, NULL);
        g_at_device_pstep  = g_at_device_step;
        g_at_device_step  += 1;
        return 0;
}

static int at_device_inc_operation (void)
{
        g_at_device_operation++;
        g_at_device_step  = 0;
        g_at_device_pstep = -1;
        gettimeofday(&g_at_device_operation_timeval, NULL);
        gettimeofday(&g_at_device_step_timeval, NULL);
        return 0;
}

static int at_device_wait_operation (uint32_t msec, int (*success) (void))
{
        int rc;
        struct timeval wait;
        struct timeval current;
        if (msec == 0) {
                return 1;
        }
        synapse_timeval_clear(&wait);
        wait.tv_sec = msec / 1000;
        wait.tv_usec = (msec % 1000) * 1000;
        synapse_timeval_add(&g_at_device_step_timeval, &wait, &wait);
        gettimeofday(&current, NULL);
        if (synapse_timeval_compare(&current, &wait, >)) {
                if (success) {
                        rc = success();
                        if (rc < 0) {
                                return -1;
                        }
                }
                return 1;
        }
        return 0;
}

static int at_device_set_state (unsigned int state)
{
        g_at_device_state             = state;
        g_at_device_state_status      = SYNAPSE_AT_DEVICE_STATE_STATUS_RUNNING;
        g_at_device_operation         = 0;
        g_at_device_step              = 0;
        g_at_device_pstep             = -1;
        gettimeofday(&g_at_device_state_timeval, NULL);
        gettimeofday(&g_at_device_operation_timeval, NULL);
        gettimeofday(&g_at_device_step_timeval, NULL);
        return 0;
}

//###########################################################################################################################

static ssize_t at_device_write_wait (const void *data, size_t count)
{
        enum status_code sc;
        sc = usart_write_buffer_wait(&g_at_device_uart_module, data, count);
        if (sc == STATUS_OK) {
                return count;
        }
        return -1;
}

static ssize_t at_device_write_stringv_wait (const char *data, va_list ap)
{
        int rc;
        int length;
        va_list vs;
        if (data == NULL) {
                return -1;
        }
        va_copy(vs, ap);
        length = vsnprintf(NULL, 0, data, vs);
        va_end(vs);
        if (length < 0) {
                return -1;
        }
        if (length + 1 >= SYNAPSE_AT_DEVICE_OUTPUT_BUFFER_SIZE) {
                synapse_errorf("string format: %s is too long: %d > %d", data, length, SYNAPSE_AT_DEVICE_OUTPUT_BUFFER_SIZE);
                return -1;
        }
        va_copy(vs, ap);
        rc = vsnprintf((char *) g_at_device_output_buffer, length + 1, data, vs);
        va_end(vs);
        if (rc < 0) {
                return -1;
        }
        return at_device_write_wait(g_at_device_output_buffer, strlen((char *) g_at_device_output_buffer));
}

static int at_device_end_command (uint32_t delay, int (*success) (void))
{
        int rc;
        rc = at_device_wait_operation(delay, NULL);
        if (rc > 0) {
                usart_abort_job(&g_at_device_uart_module, USART_TRANSCEIVER_RX);
                if (success) {
                        rc = success();
                        if (rc < 0) {
                                return -1;
                        }
                }
                return 1;
        } else if (rc < 0) {
                return -1;
        }
        return 0;
}

static int at_device_write_command (uint32_t delay, int (*success) (void), int (*error) (void), const char *command, ... )
{
        int rc;
        va_list ap;
        rc = at_device_wait_operation(delay, NULL);
        if (rc > 0) {
                usart_abort_job(&g_at_device_uart_module, USART_TRANSCEIVER_RX);
                memset(g_at_device_input_buffer, 0, SYNAPSE_AT_DEVICE_INPUT_BUFFER_SIZE);
                usart_read_buffer_job(&g_at_device_uart_module, g_at_device_input_buffer, SYNAPSE_AT_DEVICE_INPUT_BUFFER_SIZE - 1);
                va_start(ap, command);
                at_device_write_stringv_wait(command, ap);
                va_end(ap);
                if (success) {
                        rc = success();
                        if (rc < 0) {
                                return -1;
                        }
                }
                return 1;
        } else if (rc < 0) {
                if (error) {
                        rc = error();
                        if (rc < 0) {
                                return -1;
                        }
                }
                return -1;
        }
        return 0;
}

static int string_starts_with (const char *str, const char *prefix)
{
        int str_len = strlen(str);
        int prefix_len = strlen(prefix);
        return (str_len >= prefix_len) && (strncmp(str, prefix, prefix_len) == 0);
}

static int string_ends_with (const char *str, const char *suffix)
{
        int str_len = strlen(str);
        int suffix_len = strlen(suffix);
        return (str_len >= suffix_len) && (strcmp(str + (str_len - suffix_len), suffix) == 0);
}

static int at_device_wait_response_actual (int start, uint32_t duration, int (*success) (void), int (*error) (void), int (*timeout) (void), const char *response, ...)
{
        int rc;
        int ok;
        int err;
        //system_interrupt_enter_critical_section();
        //printf("g_at_device_input_buffer: '%s'\r\n", g_at_device_input_buffer);
        if (start == 1) {
                ok  = (strncmp((char *) &g_at_device_input_buffer, response, strlen(response)) == 0) ? 1 : 0;
        } else if (start == 2) {
                ok  = (strstr((char *) &g_at_device_input_buffer, response) != NULL) ? 1 : 0;
        } else {
                ok  = string_ends_with((char *) &g_at_device_input_buffer, response);
        }
        if (!ok) {
                err  = string_starts_with((char *) &g_at_device_input_buffer, "\r\nERROR\r\n");
                err |= string_starts_with((char *) &g_at_device_input_buffer, "\r\n\r\nERROR\r\n");
        }
        //system_interrupt_leave_critical_section();
        if (ok) {
                if (success) {
                        rc = success();
                        if (rc < 0) {
                                return -1;
                        }
                }
                return 1;
        } else if (err) {
                synapse_errorf("input error: '%s'\r\n", g_at_device_input_buffer);
                if (error) {
                        rc = error();
                        if (rc < 0) {
                                return -1;
                        }
                }
                return -1;
        } else if (at_device_wait_operation(duration, NULL)) {
                synapse_errorf("input timeout: '%s'\r\n", g_at_device_input_buffer);
                if (timeout) {
                        rc = timeout();
                        if (rc < 0) {
                                return -1;
                        }
                }
                return -2;
        }
        return 0;
}

/*
static int at_device_wait_response_hasstart (uint32_t duration, int (*success) (void), int (*error) (void), int (*timeout) (void), const char *response, ...)
{
        return at_device_wait_response_actual(2, duration, success, error, timeout, response);
}
*/

static int at_device_wait_response_start (uint32_t duration, int (*success) (void), int (*error) (void), int (*timeout) (void), const char *response, ...)
{
        return at_device_wait_response_actual(1, duration, success, error, timeout, response);
}


static int at_device_wait_response_end (uint32_t duration, int (*success) (void), int (*error) (void), int (*timeout) (void), const char *response, ...)
{
        return at_device_wait_response_actual(0, duration, success, error, timeout, response);
}


static int at_device_operation_error (void)
{
        g_at_device_state_status = SYNAPSE_AT_DEVICE_STATE_STATUS_ERROR;
        return 0;
}

//###########################################################################################################################

static int at_device_process_operation_powerdown_00 (void)
{
        at_device_inc_step();
        return 0;
}

static int at_device_process_operation_powerdown_01 (void)
{
        //synapse_pinmux_setup(PIN_PA00, 1, 1, 0);
        at_device_inc_step();
        return 0;
}

static int at_device_process_operation_powerdown_02 (void)
{
        at_device_wait_operation(500, at_device_inc_operation);
        return 0;
}

static struct at_device_step *g_at_device_operation_powerdown_steps[] = {
        &(struct at_device_step) { NULL, at_device_process_operation_powerdown_00 },
        &(struct at_device_step) { NULL, at_device_process_operation_powerdown_01 },
        &(struct at_device_step) { NULL, at_device_process_operation_powerdown_02 },
        NULL
};

static int at_device_process_operation_powerup_00 (void)
{
        at_device_inc_step();
        return 0;
}

static int at_device_process_operation_powerup_01 (void)
{
        //synapse_pinmux_setup(PIN_PA00, 1, 0, 0);
        at_device_inc_step();
        return 0;
}

static int at_device_process_operation_powerup_02 (void)
{
        at_device_wait_operation(4500, at_device_inc_operation);
        return 0;
}

static struct at_device_step *g_at_device_operation_powerup_steps[] = {
        &(struct at_device_step) { NULL, at_device_process_operation_powerup_00 },
        &(struct at_device_step) { NULL, at_device_process_operation_powerup_01 },
        &(struct at_device_step) { NULL, at_device_process_operation_powerup_02 },
        NULL
};

static int at_device_process_operation_reset_00 (void)
{
        at_device_inc_step();
        return 0;
}

static int at_device_process_operation_reset_01 (void)
{
        //synapse_pinmux_setup(PIN_PA00, 1, 1, 0);
        at_device_inc_step();
        return 0;
}

static int at_device_process_operation_reset_02 (void)
{
        at_device_wait_operation(500, at_device_inc_step);
        return 0;
}

static int at_device_process_operation_reset_03 (void)
{
        //synapse_pinmux_setup(PIN_PA00, 1, 0, 0);
        at_device_inc_step();
        return 0;
}

static int at_device_process_operation_reset_04 (void)
{
        at_device_wait_operation(4500, at_device_inc_operation);
        return 0;
}

static struct at_device_step *g_at_device_operation_reset_steps[] = {
        &(struct at_device_step) { NULL, at_device_process_operation_reset_00 },
        &(struct at_device_step) { NULL, at_device_process_operation_reset_01 },
        &(struct at_device_step) { NULL, at_device_process_operation_reset_02 },
        &(struct at_device_step) { NULL, at_device_process_operation_reset_03 },
        &(struct at_device_step) { NULL, at_device_process_operation_reset_04 },
        NULL
};

static int at_device_process_operation_test_00 (void)
{
        at_device_inc_step();
        return 0;
}

static int at_device_process_operation_test_01 (void)
{
        at_device_write_command(500, at_device_inc_step, at_device_operation_error, "AT+RESTORE\r\n");
        return 0;
}

static int at_device_process_operation_test_02 (void)
{
	at_device_wait_operation(1000, at_device_inc_step);
        return 0;
}

static int at_device_process_operation_test_03 (void)
{
        at_device_write_command(500, at_device_inc_step, at_device_operation_error, "AT\r\n");
        return 0;
}

static int at_device_process_operation_test_04 (void)
{
        at_device_wait_response_start(5000, at_device_inc_step, at_device_operation_error, at_device_operation_error, "AT\r\n\r\nOK\r\n");
        return 0;
}

static int at_device_process_operation_test_05 (void)
{
        at_device_write_command(500, at_device_inc_step, at_device_operation_error, "ATE0\r\n");
        return 0;
}

static int at_device_process_operation_test_06 (void)
{
        at_device_wait_response_start(5000, at_device_inc_step, at_device_operation_error, at_device_operation_error, "ATE0\r\n\r\nOK\r\n");
        return 0;
}

static int at_device_process_operation_test_07 (void)
{
        at_device_write_command(500, at_device_inc_step, at_device_operation_error, "AT+CWMODE=1\r\n");
        return 0;
}

static int at_device_process_operation_test_08 (void)
{
        at_device_wait_response_start(5000, at_device_inc_step, at_device_operation_error, at_device_operation_error, "\r\nOK\r\n");
        return 0;
}

static int at_device_process_operation_test_09 (void)
{
        at_device_write_command(500, at_device_inc_step, at_device_operation_error, "AT+CWJAP_CUR=\"SUPERONLINE-WiFi_0529\",\"JTCH7LLCNRKJ\"\r\n");
        return 0;
}

static int at_device_process_operation_test_10 (void)
{
	at_device_wait_response_start(5000, at_device_inc_step, at_device_operation_error, at_device_operation_error, "WIFI");
        return 0;
}

static int at_device_process_operation_test_11 (void)
{
        at_device_wait_response_end(5000, at_device_inc_step, at_device_operation_error, at_device_operation_error, "OK\r\n");
        return 0;
}

static int at_device_process_operation_test_12 (void)
{
        at_device_write_command(500, at_device_inc_step, at_device_operation_error, "AT+CIPSTART=\"TCP\",\"34.67.16.50\",12345\r\n");
        return 0;
}

static int at_device_process_operation_test_13 (void)
{
        at_device_wait_response_end(5000, at_device_inc_step, at_device_operation_error, at_device_operation_error, "\r\n\r\nOK\r\n");
        return 0;
}

static int at_device_process_operation_test_14 (void)
{
        at_device_write_command(500, at_device_inc_step, at_device_operation_error, "AT+CIPSEND=%d\r\n",strlen(g_at_device_output_str_buffer));
        return 0;
}

static int at_device_process_operation_test_15 (void)
{
        at_device_wait_response_start(5000, at_device_inc_step, at_device_operation_error, at_device_operation_error, "\r\nOK\r\n>");
        return 0;
}

static int at_device_process_operation_test_16 (void)
{
        at_device_write_command(500, at_device_inc_step, at_device_operation_error, "%s",g_at_device_output_str_buffer);
        return 0;
}

static int at_device_process_operation_test_17 (void)
{
        at_device_end_command(500, at_device_inc_operation);
        return 0;
}

static struct at_device_step *g_at_device_operation_test_steps[] = {
        &(struct at_device_step) { NULL, at_device_process_operation_test_00 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_01 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_02 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_03 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_04 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_05 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_06 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_07 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_08 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_09 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_10 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_11 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_12 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_13 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_14 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_15 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_16 },
        &(struct at_device_step) { NULL, at_device_process_operation_test_17 },
        NULL
};


static int at_device_process_operation_shutdown_00 (void)
{
        at_device_inc_step();
        return 0;
}

static int at_device_process_operation_shutdown_01 (void)
{
        at_device_write_command(500, at_device_inc_step, at_device_operation_error, "AT#SYSHALT\r");
        return 0;
}

static int at_device_process_operation_shutdown_02 (void)
{
        at_device_wait_response_start(5000, at_device_inc_step, at_device_operation_error, at_device_operation_error, "AT#SYSHALT\r\r\nOK\r\n");
        return 0;
}

static int at_device_process_operation_shutdown_03 (void)
{
        at_device_end_command(500, at_device_inc_operation);
        return 0;
}

static struct at_device_step *g_at_device_operation_shutdown_steps[] = {
        &(struct at_device_step) { NULL, at_device_process_operation_shutdown_00 },
        &(struct at_device_step) { NULL, at_device_process_operation_shutdown_01 },
        &(struct at_device_step) { NULL, at_device_process_operation_shutdown_02 },
        &(struct at_device_step) { NULL, at_device_process_operation_shutdown_03 },
        NULL
};

static int at_device_process_operation_success_00 (void)
{
        at_device_inc_step();
        return 0;
}

static int at_device_process_operation_success_01 (void)
{
        g_at_device_state_status = SYNAPSE_AT_DEVICE_STATE_STATUS_SUCCESS;
        at_device_inc_operation();
        return 0;
}

static struct at_device_step *g_at_device_operation_success_steps[] = {
        &(struct at_device_step) { NULL, at_device_process_operation_success_00 },
        &(struct at_device_step) { NULL, at_device_process_operation_success_01 },
        NULL
};

static struct at_device_operation *g_at_device_test_operations[] = {
        &(struct at_device_operation) { "test",               g_at_device_operation_test_steps,                     0 },
        &(struct at_device_operation) { "success",            g_at_device_operation_success_steps,                  0 },
        NULL
};

static struct at_device_operation *g_at_device_shutdown_operations[] = {
        &(struct at_device_operation) { "powerdown",          g_at_device_operation_powerdown_steps,                0 },
        &(struct at_device_operation) { "powerup",            g_at_device_operation_powerup_steps,                  0 },
        &(struct at_device_operation) { "shutdown",           g_at_device_operation_shutdown_steps,                 0 },
        &(struct at_device_operation) { "success",            g_at_device_operation_success_steps,                  0 },
        NULL,
        &(struct at_device_operation) { "powerdown",          g_at_device_operation_powerdown_steps,                0 },
        &(struct at_device_operation) { "powerup",            g_at_device_operation_powerup_steps,                  0 },
        &(struct at_device_operation) { "success",            g_at_device_operation_success_steps,                  0 },
        NULL,
        &(struct at_device_operation) { "reset",              g_at_device_operation_reset_steps,                    0 },
        &(struct at_device_operation) { "shutdown",           g_at_device_operation_shutdown_steps,                 0 },
        &(struct at_device_operation) { "success",            g_at_device_operation_success_steps,                  0 },
        NULL
};

static struct at_device_state g_at_device_states[] = {
        [SYNAPSE_AT_DEVICE_STATE_TEST]          = { "test",             g_at_device_test_operations,                0 },
        [SYNAPSE_AT_DEVICE_STATE_SHUTDOWN]      = { "shutdown",         g_at_device_shutdown_operations,            0 },
};

//###########################################################################################################################################

int synapse_at_device_init (void)
{
        unsigned int i;
        unsigned int j;
        unsigned int k;
        struct usart_config config_usart;

        memset(g_at_device_input_buffer, 0, SYNAPSE_AT_DEVICE_INPUT_BUFFER_SIZE);
        memset(&g_at_device_uart_module, 0, sizeof(struct usart_module));

        for (i = 0; i < (sizeof(g_at_device_states) / sizeof(g_at_device_states[0])); i++) {
                if (g_at_device_states[i].name == NULL) {
                        g_at_device_states[i].name = "";
                }

                g_at_device_states[i].noperations = 0;
                for (j = 0; g_at_device_states[i].operations[j]; j++) {
                        g_at_device_states[i].noperations += 1;

                        if (g_at_device_states[i].operations[j]->name == NULL) {
                                g_at_device_states[i].operations[j]->name = "";
                        }

                        g_at_device_states[i].operations[j]->nsteps = 0;
                        for (k = 0; g_at_device_states[i].operations[j]->steps[k]; k++) {
                                g_at_device_states[i].operations[j]->nsteps += 1;

                                if (g_at_device_states[i].operations[j]->steps[k]->name == NULL) {
                                        g_at_device_states[i].operations[j]->steps[k]->name = "";
                                }
                        }
                }
        }

        usart_get_config_defaults(&config_usart);
        config_usart.baudrate           = 115200;
        config_usart.generator_source   = GCLK_GENERATOR_0;
        config_usart.mux_setting        = USART_RX_1_TX_0_XCK_1;
        config_usart.pinmux_pad0        = PINMUX_PA16D_SERCOM3_PAD0; // MCU'nun Tx'i
        config_usart.pinmux_pad1        = PINMUX_PA17D_SERCOM3_PAD1; // MCU'nun Rx'i
        config_usart.pinmux_pad2        = PINMUX_UNUSED; 
        config_usart.pinmux_pad3        = PINMUX_UNUSED; 
        while (usart_init(&g_at_device_uart_module, SERCOM3, &config_usart) != STATUS_OK) {

        }
        usart_enable(&g_at_device_uart_module);

        synapse_at_device_test();

        return 0;
}

void synapse_at_device_uninit (void)
{

}

int synapse_at_device_process (void)
{
        int rc;
        unsigned int nstates = sizeof(g_at_device_states) / sizeof(g_at_device_states[0]);

        if (g_at_device_state >= nstates) {
                synapse_errorf("state: %d is out of range: %d", g_at_device_state, nstates);
                return -1;
        }

        if (g_at_device_state_status != SYNAPSE_AT_DEVICE_STATE_STATUS_RUNNING) {
                return g_at_device_state_status;
        }

        if (g_at_device_operation >= g_at_device_states[g_at_device_state].noperations) {
                synapse_errorf("state: %d, %s operation: %d is out of range: %d",
                                g_at_device_state, at_device_state_string(),
                                g_at_device_operation, g_at_device_states[g_at_device_state].noperations);
                return -1;
        }

        if (g_at_device_step >= g_at_device_states[g_at_device_state].operations[g_at_device_operation]->nsteps) {
                synapse_errorf("state: %d, %s operation: %d, %s step: %d is out of range: %d",
                                g_at_device_state, at_device_state_string(),
                                g_at_device_operation, g_at_device_states[g_at_device_state].operations[g_at_device_operation]->name,
                                g_at_device_step, g_at_device_states[g_at_device_state].operations[g_at_device_operation]->nsteps);
                return -1;
        }

        if (g_at_device_step != g_at_device_pstep) {
                if (g_at_device_step == 0) {
                        synapse_infof("at_device: %s:%s:%s",
                                        at_device_state_string(),
                                        g_at_device_states[g_at_device_state].operations[g_at_device_operation]->name,
                                        g_at_device_states[g_at_device_state].operations[g_at_device_operation]->steps[g_at_device_step]->name);
                } else {
                        synapse_infof("at_device: %s:%s:%d/%d:%s",
                                        at_device_state_string(),
                                        g_at_device_states[g_at_device_state].operations[g_at_device_operation]->name,
                                        g_at_device_step, g_at_device_states[g_at_device_state].operations[g_at_device_operation]->nsteps,
                                        g_at_device_states[g_at_device_state].operations[g_at_device_operation]->steps[g_at_device_step]->name);
                }
                g_at_device_pstep = g_at_device_step;
        }

        rc = g_at_device_states[g_at_device_state].operations[g_at_device_operation]->steps[g_at_device_step]->process();
        if (rc < 0) {
                return -1;
        }

        return g_at_device_state_status;
}

int synapse_at_device_test (void)
{
        return at_device_set_state(SYNAPSE_AT_DEVICE_STATE_TEST);
}

int synapse_at_device_shutdown (void)
{
        return at_device_set_state(SYNAPSE_AT_DEVICE_STATE_SHUTDOWN);
}

const char * at_device_state_string (void)
{
        switch (g_at_device_state) {
                case SYNAPSE_AT_DEVICE_STATE_TEST: 	        return "test";
                case SYNAPSE_AT_DEVICE_STATE_SHUTDOWN:          return "shutdown";

        }
        return "unknown";
}

unsigned int synapse_at_device_get_state (void)
{
        return g_at_device_state;
}

unsigned int synapse_at_device_get_state_status (void)
{
        return g_at_device_state_status;
}

