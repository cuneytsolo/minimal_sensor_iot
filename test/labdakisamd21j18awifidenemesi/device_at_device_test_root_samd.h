
enum  {
        SYNAPSE_AT_DEVICE_STATE_TEST               = 0,
        SYNAPSE_AT_DEVICE_STATE_SHUTDOWN           = 1,
#define SYNAPSE_AT_DEVICE_STATE_TEST               SYNAPSE_AT_DEVICE_STATE_TEST
#define SYNAPSE_AT_DEVICE_STATE_SHUTDOWN	   SYNAPSE_AT_DEVICE_STATE_SHUTDOWN
};

enum {
        SYNAPSE_AT_DEVICE_STATE_STATUS_RUNNING        = 0,
        SYNAPSE_AT_DEVICE_STATE_STATUS_SUCCESS        = 1,
        SYNAPSE_AT_DEVICE_STATE_STATUS_ERROR          = 2,
#define SYNAPSE_AT_DEVICE_STATE_STATUS_RUNNING        SYNAPSE_AT_DEVICE_STATE_STATUS_RUNNING
#define SYNAPSE_AT_DEVICE_STATE_STATUS_SUCCESS        SYNAPSE_AT_DEVICE_STATE_STATUS_SUCCESS
#define SYNAPSE_AT_DEVICE_STATE_STATUS_ERROR          SYNAPSE_AT_DEVICE_STATE_STATUS_ERROR
};

int synapse_at_device_init (void);
void synapse_at_device_uninit (void);
int synapse_at_device_process (void);

int synapse_at_device_test (void);
int synapse_at_device_shutdown (void);

const char * at_device_state_string (void);
unsigned int synapse_at_device_get_state (void);
unsigned int synapse_at_device_get_state_status (void);

