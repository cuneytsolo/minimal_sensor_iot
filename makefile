#
# ALL
# ASSIGNMENTS 
# OBJECTS
# PATHS
# FLAGS
# COMPILATION
# FINAL
# CLEAN
#

.PHONY: clean

#ALL
all  : main.bin main.hex main.eep main.lss main.srec cleanobj

#ASSIGNMENTS
CC=arm-none-eabi-gcc
LD=arm-none-eabi-g++
AR=arm-none-eabi-ar
AS=arm-none-eabi-as
OC=arm-none-eabi-objcopy

ELF  = main.elf

#OBJECTS
	# asf essential objects
OBJS += \
	interrupt_sam_nvic.o \
	init.o \
	clock.o \
	gclk.o \
	system_interrupt.o \
	pinmux.o \
	system.o \
	system_samd21.o \
	startup_samd21.o \
	syscalls.o \
	sleepmgr.o
	
	# asf project extension objects
OBJS += \
	port.o \
	sercom.o \
	sercom_interrupt.o \
	usart.o \
	usart_interrupt.o \
	rtc_count.o \
	rtc_count_interrupt.o \
	i2c_master.o \
	i2c_master_interrupt.o \
	systick_counter.o \
	tc_interrupt.o \
	tc.o

	# project code objects
OBJS += \
	main.o \
	common_system.o \
	common_pinmux.o \
	common_debug.o \
	common_console.o \
	common_clock.o \
	common_time.o \
	common_i2c.o \
	device_sht30.o \
	common_delay.o \
	device_wifi.o \
	device_hc595.o \
	common_pwm.o \
	device_button.o

#PATHS
	# asf essential paths (must include 'only header' paths too!)
INCLUDES+= \
	-I src/minimal_sensor_iot_bsp/src \
	-I src/minimal_sensor_iot_bsp/src/ASF/common/boards \
	-I src/minimal_sensor_iot_bsp/src/ASF/common/utils \
	-I src/minimal_sensor_iot_bsp/src/ASF/common/utils/interrupt \
	-I src/minimal_sensor_iot_bsp/src/ASF/common2/boards/user_board \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/system \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/system/clock \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/system/clock/clock_samd21_r21_da_ha1 \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/system/interrupt \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/system/interrupt/system_interrupt_samd21 \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/system/pinmux \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/system/power/power_sam_d_r_h \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/system/reset/reset_sam_d_r_h \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/utils \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/utils/cmsis/samd21/include \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/utils/cmsis/samd21/include\component \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/utils/cmsis/samd21/include\instance \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/utils/cmsis/samd21/include\pio \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/utils/cmsis/samd21/source \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/utils/cmsis/samd21/source/gcc \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/utils/header_files \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/utils/preprocessor \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/utils/syscalls/gcc \
	-I src/minimal_sensor_iot_bsp/src/ASF/thirdparty/CMSIS/Include \
	-I src/minimal_sensor_iot_bsp/src/ASF/thirdparty/CMSIS/Lib/GCC \
	-I src/minimal_sensor_iot_bsp/src/config \
	-I src/minimal_sensor_iot_bsp/src/ASF/common/services/sleepmgr \
	-I src/minimal_sensor_iot_bsp/src/ASF/common/services/sleepmgr/samd \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/tc \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/tc/tc_sam_d_r_h

	# asf project extension paths
INCLUDES+= \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/port \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/sercom \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/sercom/usart \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/rtc \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/rtc/rtc_sam_d_r_h \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/sercom/i2c \
	-I src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/sercom/i2c/i2c_sam0 \
	-I src/minimal_sensor_iot_bsp/src/ASF/common2/services/delay \
	-I src/minimal_sensor_iot_bsp/src/ASF/common2/services/delay/sam0

	# project code paths
INCLUDES+= \
	-I src/minimal_sensor_iot_application/src

INCLUDES+= \
	-I src/minimal_sensor_iot_common/src

INCLUDES+= \
	-I src/minimal_sensor_iot_device/src
	
# FLAGS
	# compiler flags
CFLAGS+= \
	-Wall \
	-Werror \
	-Wno-error=format-zero-length \
	-Wno-format-zero-length \
	-Wno-error=unused-parameter \
	-Wno-unused-parameter \
	-pipe \
	-Os \
	-fdata-sections \
	-ffunction-sections \
	-mlong-calls \
	-fno-strict-aliasing \
	-ffunction-sections \
	-fdata-sections \
	--param max-inline-insns-single=500

CFLAGS+= \
	-mthumb \
	-mcpu=cortex-m0plus \
	-D__SAMD21G18AU__ \
	-DBOARD=USER_BOARD \
	-DARM_MATH_CM0PLUS=true \
	-DSYSTICK_MODE \
	-DADC_CALLBACK_MODE=true \
	-DI2C_MASTER_CALLBACK_MODE=true \
	-DSPI_CALLBACK_MODE=true \
	-DUSART_CALLBACK_MODE=true \
	-DTC_ASYNC=true \
	-DWDT_CALLBACK_MODE=true \
	-DRTC_COUNT_ASYNC=true

CFLAGS+= $(INCLUDES)

	# linker flags
LDFLAGS+= \
	-mthumb \
	--specs=nano.specs \
	-Wl,--start-group \
	-larm_cortexM0l_math \
	-Wl,--end-group \
	-Lsrc/minimal_sensor_iot_bsp/src/ASF/thirdparty/CMSIS/Lib/GCC  \
	-lm  \
	-Wl,--gc-sections \
	-mcpu=cortex-m0plus \
	-Wl,--entry=Reset_Handler \
	-mthumb \
	-Tsrc/minimal_sensor_iot_bsp/src/ASF/sam0/utils/linker_scripts/samd21/gcc/samd21g18au_flash.ld

# COMPILATION
	# asf essential compilation
%.o:    src/minimal_sensor_iot_bsp/src/ASF/common/utils/interrupt/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/common2/boards/user_board/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/system/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/system/clock/clock_samd21_r21_da_ha1/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/system/interrupt/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/system/pinmux/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/sam0/utils/cmsis/samd21/source/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/sam0/utils/cmsis/samd21/source/gcc/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/sam0/utils/syscalls/gcc/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/common/services/sleepmgr/samd/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"	

	# asf project extension compilation
%.o:    src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/port/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/sercom/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/sercom/usart/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/rtc/rtc_sam_d_r_h/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/sercom/i2c/i2c_sam0/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/common2/services/delay/sam0/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/tc/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_bsp/src/ASF/sam0/drivers/tc/tc_sam_d_r_h/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"

	# project code compilation
%.o:    src/minimal_sensor_iot_application/src/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_common/src/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
%.o:    src/minimal_sensor_iot_device/src/%.c
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@:$^"
	
# FINAL
$(ELF)  : $(OBJS)
	@$(LD) $(LDFLAGS) -o $@ $(OBJS)
	@echo "$@:$<"

main.bin: main.elf
	$(OC) -O binary main.elf main.bin
	@echo "$@:$<"
main.hex: main.elf
	${OC} -O ihex -R .eeprom -R .fuse -R .lock -R .signature main.elf main.hex
	@echo "$@:$<"
main.eep: main.elf
	${OC} -j .eeprom --set-section-flags=.eeprom=alloc,load --change-section-lma .eeprom=0 --no-change-warnings -O binary main.elf main.eep
	@echo "$@:$<"
main.lss: main.elf
	${OC} -S main.elf main.lss
	@echo "$@:$<"
main.srec: main.elf
	${OC} -O srec -R .eeprom -R .fuse -R .lock -R .signature main.elf main.srec
	@echo "$@:$<"

# CLEAN
cleanobj :
	rm *.o

clean :
	rm *.o *.d *.elf *.bin *.hex *.eep *.lss *.srec
