# minimal_sensor_iot

# download

## dependencies

    apt install git

## clone

    git clone --recursive git@bitbucket.org:cuneytsolo/minimal_sensor_iot.git

# build

## dependencies

### arm toolchain

    curl -O -L http://ww1.microchip.com/downloads/secure/en/devicedoc/arm-gnu-toolchain-6.3.1.508-linux.any.x86_64.tar.gz
    tar -zxvf arm-gnu-toolchain-6.3.1.508-linux.any.x86_64.tar.gz

## make

    cd minimal_sensor_iot
    PATH=$PATH:/path/to/arm-none-eabi/bin make

# program

## flash

## application

    sudo openocd -f contrib/openocd-segger-program-application.cfg


